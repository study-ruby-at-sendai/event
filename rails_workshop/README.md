# 「Ruby on Rails」ではじめるWEB開発

[Rails on Rails Tutorial](https://railstutorial.jp/)の第一章の内容を一部変更して、ワークショップを実施します。

変更がある場所は以下の通りです。

- 1.2.1 開発環境
- 1.3 最初のアプリケーション
- 1.3.1 rails server
- 1.4.3 Bitbucket
- 1.5.1 Herokuのセットアップ

## 1.2.1 開発環境

開発環境には、Cloud IDEの[Codenvy](https://codenvy.io)を使用します。
Rails Tutorialの内容であれば無料で利用できるので、サインアップしてログインしてください。

ログインしたら「Dashboard」の「Create Workspace」をクリックします。

![create workspace 1](rails_workshop/create_workspace_1.png)

SELECT STACKのリストの「Rails」を選択して「Create」をクリックします。

![create workspace 2](rails_workshop/create_workspace_2.png)

しばらく待つとRailsの開発環境のクラウドIDEが起動します。
クラウドIDEは大きく分けて「プロジェクトエクスプローラー」、「テキスト編集エリア」、「ターミナル」の３つの領域に分かれています。

![ide 1](rails_workshop/ide_1.png)

Rubyではインデントのサイズは2が一般的なのでエディターの設定を変更します。
画面上部の「Profile」>「Preferences」>「IDE」>「Editor」の「Tab Size」を2に変更します。

## 1.3 最初のアプリケーション

Railsプロジェクトを作成するディレクトリは`/projects`にする必要があります。

アプリケーションの作成は以下のコマンドで実行して下さい。

```bash
$ cd /projects
$ rails _5.1.4_ new hello_app
```
## 1.3.2 rails server

クラウドIDEでアプリケーションを実行するには「Run」ボタンをクリックして下さい。

![ide 2](rails_workshop/ide_2.png)

画面下部に「run」タブが開きます。その中の「preview」のURLでアプリケーションにアクセスできます。
また、アプリケーションを停止する場合は、画面右上の「Stop」ボタンをクリックして下さい。

![ide 3](rails_workshop/ide_3.png)

## 1.4.3 Bitbucket

Bitbucketの代わりにGitLabを使います。
GitLabを利用するにはまず[GitLab.com](https://gitlab.com)にアクセスし、画面右上の「Sign In/Register」をクリックします。
アカウントを持っていない場合は、「Register」タブに切り替えてアカウントを登録する必要があります。
なお、Google AccountとTwitterとGitHubとBitbucketのアカウントでもログインができるので、好みに合わせて利用して下さい。

SSHの公開鍵の登録は画面右上のアバターアイコンをクリック > Settings > SSH Keysで行うことができます。
「Key」にSSHの公開鍵を貼り付けて、「Title」に適当な名前を入力して「Add key」をクリックして下さい。

![gitlab ssh keys 1](rails_workshop/gitlab_ssh_keys_1.png)

続いてGitLabでプロジェクトを作成します。
プロジェクトを作成するには、まず左上のGitLabのアイコンをクリックしてトップページへ移動してから「Create a project」ボタンをクリックします。

![gitlab create project 1](rails_workshop/gitlab_create_project_1.png)

「Project name」に"hello_app"と入力して、「Create project」ボタンをクリックします。

![gitlab create project 2](rails_workshop/gitlab_create_project_2.png)

GitLabのアカウントを登録しないで、Googleアカウントなどを利用してログインした場合は、パスワードを設定するように警告が表示されます。
今回はSSHでpushやpullをするので無視しても問題ないので「Don't show again」をクリックして非表示にして下さい。

![gitlab password warning 1](rails_workshop/gitlab_password_warning_1.png)

Over viewにGitのリポジトリのURLが表示されますが、HTTPSが選択されている場合はSSHに切り替えてください。
SSHになっていることを確認してURL右側のクリップボードアイコンをクリックして下さい。

![gitlab url 1](rails_workshop/gitlab_url_1.png)

続いてクラウドIDEのターミナルで以下のコマンドを実行して、リモートリポジトリとしてGitLabを追加しpushします。

```sh
$ git remote add origin <GitLabでコピーしたURL>
$ git push -u origin --all
```

以上でGitLabのプロジェクトにhello_appのリポジトリが表示されます。

## 1.5.1 Herokuのセットアップ

リスト 1.14の方法ではインストールでエラーが発生するので、以下のように[Standalone installation](https://devcenter.heroku.com/articles/heroku-cli#standalone-installation)を行う必要があります。

```sh
$ cd /tmp # tmpディレクトリで作業する
$ wget https://cli-assets.heroku.com/heroku-cli/channels/stable/heroku-cli-linux-x64.tar.gz -O heroku.tar.gz
$ tar -xvzf heroku.tar.gz
$ mkdir -p /usr/local/lib /usr/local/bin
$ sudo mv heroku-cli-v6.16.16-afdd2a0-linux-x64 /usr/local/lib/heroku
$ sudo ln -s /usr/local/lib/heroku/bin/heroku /usr/local/bin/heroku
$ cd /projects/hello_app # 元のディレクトリへ戻る
```
